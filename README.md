# Cachy Plugin - README #
---

### Overview ###

**Cachy** is a plugin for [**Bludit**](https://www.bludit.com/), a nice little and slim CMS written in PHP. This Plugin will cache the Bludit HTML pages and therefore the code is delivered faster. On the settings panel you can configure the plugin and exclude some pages if you don't want them to be cached. The plugin also minifies the HTML code before writing it to the cache. The **Cachy** plugin can display a marker in the head of each cached page (HTML comment) which shows the details about the cache file (UUID, creation- and expiration time). With the [LOADTIME] placeholder you can display the loadtime in the footer of your website (for testing). There is also a button to empty the cache and start over. Be aware: Bludit is a fast neat CMS (...and that's the reason why I use it) and if you only have one or two sentences of text on each of your 3 pages, this plugin probably will not speed up your site very much because Bludit is already fast. If your pages are bigger and you have a lot of them, this Plugin can really increase the loading time.

### Screenshots ###

![Cachy - Plugin settings](development/readme/cachy_1.png "Cachy - Plugin settings")
![Cachy - Cached HTML code](development/readme/cachy_2.png "Cachy - Cached HTML code")
![Cachy - Load time in footer](development/readme/cachy_3.png "Cachy - Load time in footer")

### Setup ###

* Copy the directory **cachy** to the plugin directory of your Bludit installation.
* Normally this is under **bludit/bl-plugins/**.
* The plugin folder would be under **bludit/bl-plugins/cachy**.
* Go to the Bludit administration page and select **Plugins** in the left sidebar.
* You should see the plugin installed there under the name **Cachy**.
* Click on the link below the plugin name to enable the plugin.
* Click on the link **Settings** to configure the plugin.
* If you want, enter the placeholder/shortcode [LOADTIME] in the footer of your pages.

### Support ###

This is a free plugin and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Cachy** plugin is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
