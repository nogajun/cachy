<?php

/**
 *  Cachy
 *
 *  @package Bludit
 *  @subpackage Plugins
 *  @author PB-Soft - Patrick Biegel
 *  @copyright 2019 PB-Soft
 *  @version 3.1
 *  @release 2019-07-03
 *
 */

class pluginCachy extends Plugin {

	// Method to initialize the plugin.
	public function init()
	{

    // Specify the used database fields.
    $this->dbFields = array(
      'cache_duration' => '15', // Specify the cache duration in minutes.
      'cache_exclude'  => '',   // Specify the pages which are excluded from caching.
      'cache_marker'   => true, // Specify if a cache marker will be inserted into the HTML code.
      'load_time'      => true, // Specify if the page load time should be displayed on the pages.
      'cache_minify'   => true  // Specify if the cached HTML code should be minified.
    );
	}

  // Method/function to process the post settings.
	public function post()
	{

    // Process the POST variables like in the parent function.
    // Code from file: bludit/bl-kernel/abstract/plugin.class.php
    $args = $_POST;
    foreach ($this->dbFields as $key=>$value) {
      if (isset($args[$key])) {
        $value = Sanitize::html($args[$key]);
        if ($value === 'false') {
          $value = false;
        } elseif ($value === 'true') {
          $value = true;
        }
        settype($value, gettype($this->dbFields[$key]));
        $this->db[$key] = $value;
      }
    }

    // Check if the cache should be deleted (delete button was pressed).
    if (isset($_POST['cache_delete']) && $_POST['cache_delete'] == 1) {

      // Check if the cache directory is writable.
      if (is_writable(PATH_PLUGINS."cachy/cache/")) {

        // Delete the cache content (all HTML files).
        array_map('unlink', glob(PATH_PLUGINS."cachy/cache/*.html"));

        // Return a success.
        return true;

        // The cache directory is not writable.
      } else {

        // Return an error.
        return false;
      }

      // The cache should not be deleted (delete button was not pressed).
    } else {

      // Save the plugin settings to the database.
      return $this->save();
    }
  }

	// Method/function to display the plugin administration page.
	public function form()
	{

    // Set the language variable global.
    global $L;

    // Cache duration input field.
    $html  = '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('cache-duration').'</label>';
    $html .= '<input name="cache_duration" id="cache_duration" type="text" placeholder="15" value="'.$this->getValue('cache_duration').'">';
    $html .= '<p>'.$L->get('cache-duration-tip').'</p>';
    $html .= '</div>';

    // Excluded pages input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('cache-exclude').'</label>';
    $html .= '<input name="cache_exclude" id="cache_exclude" type="text" placeholder="'.$L->get('cache-exclude-placeholder').'" value="'.$this->getValue('cache_exclude').'">';
    $html .= '<p>'.$L->get('cache-exclude-tip').'</p>';
    $html .= '</div>';

    // Cache marker selector.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('cache-marker').'</label>';
    $html .= '<select name="cache_marker">';
    $html .= '<option value="true" '.($this->getValue('cache_marker')===true?'selected':'').'>'.$L->get('cache-marker-enabled').'</option>';
    $html .= '<option value="false" '.($this->getValue('cache_marker')===false?'selected':'').'>'.$L->get('cache-marker-disabled').'</option>';
    $html .= '</select>';
    $html .= '<p>'.$L->get('cache-marker-tip').'</p>';
    $html .= '</div>';

    // Load time selector.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('load-time').'</label>';
    $html .= '<select name="load_time">';
    $html .= '<option value="true" '.($this->getValue('load_time')===true?'selected':'').'>'.$L->get('load-time-enabled').'</option>';
    $html .= '<option value="false" '.($this->getValue('load_time')===false?'selected':'').'>'.$L->get('load-time-disabled').'</option>';
    $html .= '</select>';
    $html .= '<p>'.$L->get('load-time-tip').'</p>';
    $html .= '</div>';

    // Minify HTML selector.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('cache-minify').'</label>';
    $html .= '<select name="cache_minify">';
    $html .= '<option value="true" '.($this->getValue('cache_minify')===true?'selected':'').'>'.$L->get('cache-minify-enabled').'</option>';
    $html .= '<option value="false" '.($this->getValue('cache_minify')===false?'selected':'').'>'.$L->get('cache-minify-disabled').'</option>';
    $html .= '</select>';
    $html .= '<p>'.$L->get('cache-minify-tip').'</p>';
    $html .= '</div>';

    // Create a new form row for the 'delete cache' button.
    $html .= '<div>';

    // Add a label for the cache delete button.
    $html .= '<label for="jscache_delete"  style="font-weight:bold">'.$L->get('cache-delete').'</label>';

    // Add a box for the button form control.
    $html .= '<div>';

    // Add the 'delete cache' button.
    $html .= '<button id="jscache_delete" style="font-weight:bold;margin-left:0" name="cache_delete" type="submit" value="1">'.$L->get('cache-delete-button').'</button>';

    // Add the description for the 'delete cache' button.
    $html .= '<p>'.$L->get('cache-delete-tip').'</p>';

    // Close the form control box.
    $html .= '</div>';

    // Close the form row.
    $html .= '</div>';

    // Return the HTML code.
    return $html;
	}

	// Method/function to start the caching.
	public function beforeSiteLoad()
	{

    // Specify the global variables.
    global $L, $page, $page_cached, $position, $actual_time, $cache_filename, $loadTime;

    // Get the cache exclude setting from the database.
    $cache_exclude  = $this->getValue('cache_exclude');

    // Search for the actual page in the excluded pages string.
    $position = strpos(strtolower($cache_exclude), $page->key());

    // Check if the actual page is not on the excluded page list.
    if ($position === false)  {

      // Specify the cache filename.
      $cache_filename = PATH_PLUGINS."cachy/cache/".sha1($_SERVER['REQUEST_URI']).".html";

      // Get the cache duration setting from the database.
      $cache_duration = $this->getValue('cache_duration');

      // Get the actual timestamp.
      $actual_time = time();

      // Initialize the 'page_cached' flag.
      $page_cached = 0;

      // Check if a cached version of the actual file exist.
      if (file_exists($cache_filename)) {

        // Get the modified time of the cache file.
        $cache_file_created = filemtime($cache_filename);

        // Calculate the expiration time for the cache file.
        $cache_file_expiration = $cache_file_created + $cache_duration * 60;

        // Check if the cache file is still valid.
        if ($cache_file_expiration > $actual_time) {

          // Enable the 'page_cached' flag.
          $page_cached = 1;

          // Get the content of the cache file.
          $page_content = file_get_contents($cache_filename);

          // Get the load time setting from the database.
          $load_time = $this->getValue('load_time');

          // Check if the load time should be displayed.
          if ($load_time == 1) {

            // Calculate the load time of the cache file.
            $loadTime_ms = round((microtime(true) - $loadTime) * 1000, 2);

            // Insert the load time into the actual HTML code.
            $page_content = str_replace("[LOADTIME]", $L->get('load-time-string').$loadTime_ms." ms".$L->get('cached-page'), $page_content);
          }

          // Display the content of the cached page.
          echo $page_content;

          // The page is displayed - Time to exit.
          exit();
        }
      }
    }

    // Start the output buffer.
    ob_start();
  }

	// Method/function to process the caching after the page has loaded.
	public function afterSiteLoad()
	{

    // Specify the global variables.
    global $L, $page, $page_cached, $position, $actual_time, $cache_filename, $loadTime;

    // Get the actual page content.
    $page_content = ob_get_clean();

    // Get the cache minify setting from the database.
    $cache_minify = $this->getValue('cache_minify');

    // Check if the HTML code should be minified.
    if ($cache_minify == 1) {

      // Replace '&nbsp;' in the page content with whitespaces.
      $page_content = str_replace("&nbsp;", " ", $page_content);

      // Replace tabs in the page content with whitespaces.
      $page_content = preg_replace("/\t+/", " ", $page_content);

      // Replace multiple whitespaces in the page content with one whitespace.
      $page_content = preg_replace("/ {2,}/", " ", $page_content);

      // Remove all JS comments from the code.
      $page_content = preg_replace('%((?:\\/\\*(?:[^*]|(?:\\*+[^*\\/]))*\\*+\\/)|(?:(?<![":\\\\])\\/\\/.*))%', "", $page_content);

      // Replace HTML comments in the page content .
      $page_content = preg_replace('/(?:<!--(?:[^>]|(?:-+[^>]))*-->)/m', '', $page_content);

      // Remove whitespaces/tabs/linebreaks between > and < characters from the page content.
      $page_content = preg_replace('/>[\s]+</m', '><', $page_content);

      // Remove linebreaks in the page content .
      $page_content = preg_replace("/[\r\n]+/", "", $page_content);

      // Put JavaScript functions on a new line.
      $page_content = str_replace("function ", "\r\nfunction ", $page_content);
    }

    // Check if the actual page is not cached and is not on the excluded page list.
    if ($page_cached == 0 && $position === false)  {

      // Check if the cache directory exist.
      if (!file_exists(PATH_PLUGINS."cachy/cache/")) {

        // Create the cache directory.
        mkdir(PATH_PLUGINS."cachy/cache/");
      }

      // Check if the cache directory is writable.
      if (is_writable(PATH_PLUGINS."cachy/cache/")) {

        // Get the cache duration setting from the database.
        $cache_duration = $this->getValue('cache_duration');

        // Get the cache marker setting from the database.
        $cache_marker   = $this->getValue('cache_marker');

        // Check if a cache marker should be inserted into the HTML code.
        if ($cache_marker == 1) {

          // Calculate the cache file expiration timestamp.
          $cache_file_expiration = $actual_time + $cache_duration * 60;

          // Create the marker code.
          $marker_content  = "\n";
          $marker_content .= "<!-- =========================================================\n";
          $marker_content .= "  Page cached by:   Cachy - The cache plugin for Bludit\n";
          $marker_content .= "  Page indentifier: ".sha1($_SERVER['REQUEST_URI'])."\n";
          $marker_content .= "  Requested page:   ".$_SERVER['REQUEST_URI']."\n";
          $marker_content .= "  Caching time:     ".date("d.m.Y H:i:s")."\n";
          $marker_content .= "  Expiration time:  ".date("d.m.Y H:i:s", $cache_file_expiration)."\n";
          $marker_content .= "========================================================== -->\n";

          // Insert the marker code into the HTML code.
          $page_content = str_replace("<head>", "<head>".$marker_content, $page_content);
        }

        // Write the page content into a new cache file.
        file_put_contents($cache_filename, $page_content);
      }
    }

    // Get the load time setting from the database.
    $load_time = $this->getValue('load_time');

    // Check if the load time should be displayed.
    if ($load_time == 1) {

      // Calculate the load time in milliseconds.
      $loadTime_ms = round((microtime(true) - $loadTime) * 1000, 0);

      // Insert the load time into the actual HTML code.
      $page_content = str_replace("[LOADTIME]", $L->get('load-time-string').$loadTime_ms." ms".$L->get('not-cached-page'), $page_content);
    }

    // Display the content of the 'normal' page (not from the cache).
    echo $page_content;

    // The page is displayed - Time to exit.
    exit();
	}
}
